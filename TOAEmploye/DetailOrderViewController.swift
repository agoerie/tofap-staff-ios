//
//  DetailOrderViewController.swift
//  TOAEmploye
//
//  Created by Agoerie on 2/23/18.
//  Copyright © 2018 ykdigital. All rights reserved.
//

import UIKit
import SwiftyJSON
import HCSStarRatingView

protocol detailOrderDelegate: class {
    func cancelOrder(controller: DetailOrderViewController)
}

class DetailOrderViewController: UIViewController {
    
    @IBOutlet weak var table: UITableView!{
        didSet{
            let xib = DetailOrderCell.nib
            table.register(xib, forCellReuseIdentifier: DetailOrderCell.identifier)
            
            table.backgroundColor = UIColor.init(hexString: "ebebeb")
            
            table.delegate = self
            table.dataSource = self
            
            table.separatorStyle = .none
            table.tableFooterView = UIView()
        }
    }
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblCourier: UILabel!
    @IBOutlet var lblStatus: UILabel!
    @IBOutlet var viewRating: HCSStarRatingView!
    @IBOutlet var constrainCancelHeight: NSLayoutConstraint!
    
    var listOrder: ListOrder?
    
    var detailOrder = [DetailOrder]() {
        didSet {
            temps = detailOrder
            table.reloadData()
        }
    }
    
    var temps :[DetailOrder] = []
    var idLogin: String = "0"
    
    open weak var delegate: detailOrderDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.isNavigationBarHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setupView() {
        lblDate.text = listOrder?.tglPemesanan
        lblCourier.text = listOrder?.kurir
        lblStatus.text = listOrder?.txtStat
        viewRating.value = CGFloat((listOrder?.rate)!)
        viewRating.starBorderColor = UIColor.lightGray
        viewRating.tintColor = UIColor.init(hexString: "FFFF00")
        
        if listOrder?.txtStat == "Cancel" || listOrder?.txtStat == "Done" {
            constrainCancelHeight.constant = 0
        }
        
        if let userData = UserDefaults.standard.string(forKey: "userdata") {
            let jsonArray = JSON.parse(userData).arrayValue
            idLogin = jsonArray[0].stringValue
        }
        
        getDetailData()
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func cancelAction(_ sender: UIButton) {
        OrderController().postCancelOrder(param: ["id":"\((listOrder?.idPemesanan)!)","id_user":idLogin], onSuccess: { (code, message, result) in
            print("Success cancel order")
        }, onFailed: { (message) in
            print(message)
            print("Do action when data failed to fetching here")
        }) { (message) in
            print(message)
            print("Do action when data complete to fetching here")
            let alert = UIAlertController.init(title: "Sukses", message: "Pemesanan anda berhasil dibatalkan", preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: { (action) in
                self.delegate?.cancelOrder(controller: self)
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func getDetailData() {
        OrderController().getDetailOrder(id: "\((listOrder?.idPemesanan)!)", onSuccess: { (code, message, results) in
            guard let res = results else { return }
            
            self.detailOrder = res
        }, onFailed: { (message) in
            print(message)
            print("Do action when data failed to fetching here")
        }) { (message) in
            print(message)
            print("Do action when data complete to fetching here")
        }
    }

}

extension DetailOrderViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return detailOrder.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = detailOrder[indexPath.row]
        
        return DetailOrderCell.configure(context: self, tableView: tableView, indexPath: indexPath, object: data)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension DetailOrderViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
}
