//
//  LoginController.swift
//  TOAEmploye
//
//  Created by Agoerie on 2/16/18.
//  Copyright © 2018 ykdigital. All rights reserved.
//

import UIKit
import SwiftyJSON
import AnimatedTextInput
import IHKeyboardAvoiding

class LoginController: UIViewController {

    @IBOutlet var tfUserName: AnimatedTextInput!
    @IBOutlet var tfPassword: AnimatedTextInput!
    @IBOutlet var loginBtn: UIButton!
    
    var activityIndicator: UIActivityIndicatorView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setupView() {
        let gesture = UITapGestureRecognizer.init(target: self, action: #selector(resignResponder))
        view.addGestureRecognizer(gesture)
        
        loginBtn.layer.cornerRadius = 22.5
        
        tfUserName.placeHolderText = "Username"
        tfUserName.type = .standard
        tfUserName.style = CustomTextInputLoginStyle() as AnimatedTextInputStyle
        
        tfPassword.placeHolderText = "Password"
        tfPassword.type = .password
        tfPassword.style = CustomTextInputLoginStyle() as AnimatedTextInputStyle
        
        activityIndicator = UIActivityIndicatorView.init(activityIndicatorStyle: .whiteLarge)
        activityIndicator?.center = view.center
        activityIndicator?.hidesWhenStopped = true
        activityIndicator?.stopAnimating()
        
        view.addSubview(activityIndicator!)
    }

    @IBAction func loginAction(_ sender: UIButton) {
        activityIndicator?.startAnimating()
        loginBtn.isEnabled = false
        
        UserController().postLogin(param: ["username":tfUserName.text!,"password":tfPassword.text!], onSuccess: { (code, message, results) in
            guard let data = results else { return }
            
            let jsonString = JSON(data).rawString()
            UserDefaults.standard.set(jsonString, forKey: "userdata")
            
            self.activityIndicator?.stopAnimating()
            self.loginBtn.isEnabled = true
        }, onFailed: { (message) in
            print(message)
            print("Do action when data failed to fetching here")
            self.activityIndicator?.stopAnimating()
            self.loginBtn.isEnabled = true
            
            let alert = UIAlertController.init(title: nil, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }) { (message) in
            print(message)
            print("Do action when data complete to fetching here")
            self.toHomeScreen()
        }
    }
    
    @IBAction func forgotAction(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: REST.StoryboardReferences.main, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.User.forgot) as! ForgotPasswordController
        
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func registerAction(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: REST.StoryboardReferences.main, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.User.register) as! RegisterViewController
        
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func resignResponder() {
        self.view.endEditing(true)
    }
    
    func toHomeScreen() {
        let storyboard = UIStoryboard(name: REST.StoryboardReferences.order, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.Order.list) as! ListOrderViewController
        let nc = UINavigationController.init(rootViewController: vc)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = nc
        appDelegate.window?.makeKeyAndVisible()
    }
    
}

struct CustomTextInputLoginStyle: AnimatedTextInputStyle {
    let placeholderInactiveColor = UIColor.white
    let activeColor = UIColor.white
    let inactiveColor = UIColor.white.withAlphaComponent(1.0)
    let lineInactiveColor = UIColor.white.withAlphaComponent(1.0)
    let lineActiveColor = UIColor.white.withAlphaComponent(1.0)
    let lineHeight: CGFloat = 1
    let errorColor = UIColor.red
    let textInputFont = UIFont.systemFont(ofSize: 17)
    let textInputFontColor = UIColor.white
    let placeholderMinFontSize: CGFloat = 9
    let counterLabelFont: UIFont? = UIFont.systemFont(ofSize: 9)
    let leftMargin: CGFloat = 25
    let topMargin: CGFloat = 15
    let rightMargin: CGFloat = 0
    let bottomMargin: CGFloat = 0
    let yHintPositionOffset: CGFloat = 7
    let yPlaceholderPositionOffset: CGFloat = 0
    public let textAttributes: [String: Any]? = nil
}
