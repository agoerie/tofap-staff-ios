//
//  FloorViewController.swift
//  TOAEmploye
//
//  Created by Agoerie on 3/3/18.
//  Copyright © 2018 ykdigital. All rights reserved.
//

import UIKit

protocol floorViewDelegate: class {
    func onDismiss(controller: FloorViewController)
    func onSelectMenu(controller: FloorViewController,menu: String,id: Int)
}

class FloorViewController: UIViewController {
    
    @IBOutlet weak var table: UITableView!{
        didSet{
            let xib = FloorCell.nib
            table.register(xib, forCellReuseIdentifier: FloorCell.identifier)
            
            table.delegate = self
            table.dataSource = self
            table.allowsSelection = true
            
            table.separatorStyle = .none
            table.tableFooterView = UIView()
        }
    }
    
    @IBOutlet var tableConstraint: NSLayoutConstraint!
    
    open weak var delegate: floorViewDelegate?
    
    let data = ["First Floor","Second Floor","Third Floor","Fourth Floor","Fifth Floor"]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableConstraint.constant = CGFloat(data.count * 45)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func closeAction(_ sender: UIButton) {
        delegate?.onDismiss(controller: self)
    }

}

extension FloorViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return FloorCell.configure(context: self, tableView: tableView, indexPath: indexPath, object: data)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.onSelectMenu(controller: self, menu: data[indexPath.row], id: indexPath.row + 1)
    }
}

extension FloorViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
}
