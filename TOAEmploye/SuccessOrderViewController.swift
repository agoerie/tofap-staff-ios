//
//  SuccessOrderViewController.swift
//  TOAEmploye
//
//  Created by Agoerie on 2/27/18.
//  Copyright © 2018 ykdigital. All rights reserved.
//

import UIKit

class SuccessOrderViewController: UIViewController {

    @IBOutlet var btnBack: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setupView() {
        btnBack.layer.borderWidth = 1
        btnBack.layer.borderColor = UIColor.white.cgColor
        btnBack.layer.cornerRadius = 20
    }

    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
        
        NotificationCenter.default.post(name: Notification.Name("reloadDataListOrder"), object: nil)
    }
}
