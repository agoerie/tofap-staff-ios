//
//  ReportViewController.swift
//  TOAEmploye
//
//  Created by Agoerie on 3/21/18.
//  Copyright © 2018 ykdigital. All rights reserved.
//

import UIKit
import WebKit
import KYNavigationProgress

class ReportViewController: UIViewController {
    
    @IBOutlet var viewNavBar: UIView!
    @IBOutlet var viewSeparator: UIView!
    
    var link: String?
    var webView: WKWebView?
    
    override func loadView() {
        super.loadView()
        
        webView = WKWebView.init(frame: CGRect.init(x: 0, y: 45, width: view.bounds.width, height: view.bounds.height - 45))
        view.addSubview(webView!)
        view.bringSubview(toFront: viewNavBar)
        view.bringSubview(toFront: viewSeparator)
        webView?.navigationDelegate = self
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        webView?.removeObserver(self, forKeyPath: "estimatedProgress")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setupView() {
        let url = URL.init(string: link!)
        let req = URLRequest.init(url: url!)
        webView?.load(req)
        
        webView!.addObserver(self, forKeyPath: "estimatedProgress", options: .new, context: nil)
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

}

extension ReportViewController: WKNavigationDelegate {
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "estimatedProgress" {
            if self.webView?.estimatedProgress == 1 {
                self.navigationController!.finishProgress()
            } else {
                self.navigationController?.setProgress(Float(self.webView!.estimatedProgress), animated: true)
            }
        }
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        decisionHandler(.allow)
    }
}
