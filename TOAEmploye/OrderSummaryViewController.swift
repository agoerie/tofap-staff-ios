//
//  OrderSummaryViewController.swift
//  TOAEmploye
//
//  Created by Agoerie on 2/26/18.
//  Copyright © 2018 ykdigital. All rights reserved.
//

import UIKit
import SwiftyJSON

class OrderSummaryViewController: UIViewController {
    
    @IBOutlet weak var table: UITableView!{
        didSet{
            let xib = OrderSummaryCell.nib
            table.register(xib, forCellReuseIdentifier: OrderSummaryCell.identifier)
            
            table.delegate = self
            table.dataSource = self
            
            table.separatorStyle = .none
            table.tableFooterView = UIView()
        }
    }
    @IBOutlet var btnConfirm: UIButton!
    
    var listItem: [ListItem] = []
    var idLogin: String = "0"
    var activityIndicator: UIActivityIndicatorView?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setupView() {
        if let userData = UserDefaults.standard.string(forKey: "userdata") {
            let jsonArray = JSON.parse(userData).arrayValue
            idLogin = jsonArray[0].stringValue
        }
        
        activityIndicator = UIActivityIndicatorView.init(activityIndicatorStyle: .gray)
        activityIndicator?.center = view.center
        activityIndicator?.hidesWhenStopped = true
        activityIndicator?.stopAnimating()
        
        view.addSubview(activityIndicator!)
    }

    @IBAction func confirmAction(_ sender: UIButton) {
        let alert = UIAlertController.init(title: "Konfirmasi", message: "Apakah anda yakin?", preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Lanjutkan", style: .default, handler: { (action) in
            self.orderAction()
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func orderAction() {
        activityIndicator?.startAnimating()
        btnConfirm.isEnabled = false
        
        let param = ["mode":"add","id_pemesan":idLogin,"item":getParameters(),"device":"IOS"] as [String : Any]
        
        OrderController().postCreateOrder(param: param, onSuccess: { (code, message, result) in
            print(message)
            print("Do action when data success to fetching here")
            
            self.activityIndicator?.stopAnimating()
            self.btnConfirm.isEnabled = true
        }, onFailed: { (message) in
            print(message)
            print("Do action when data failed to fetching here")
            
            self.activityIndicator?.stopAnimating()
            self.btnConfirm.isEnabled = true
        }) { (message) in
            print(message)
            print("Do action when data complete to fetching here")
            
            let storyboard = UIStoryboard(name: REST.StoryboardReferences.order, bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.Order.successOrder) as! SuccessOrderViewController
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func getParameters() -> [String: [String: Any]] {
        var result = [String: [String: Any]]()
        
        let mmaps = listItem
        for i in 0..<mmaps.count {
            let mmap = mmaps[i]
            let material: [String: Any] = [
                "id_item": mmap.id,
                "qty": mmap.qtyGet,
                "note": mmap.note
            ]
            result["\(i)"] = material
        }
        
        return result
    }
    
}

extension OrderSummaryViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = listItem[indexPath.row]
        
        return OrderSummaryCell.configure(context: self, tableView: tableView, indexPath: indexPath, object: data)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension OrderSummaryViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 170
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
