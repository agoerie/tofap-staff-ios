//
//  ListItem.swift
//  TOAEmploye
//
//  Created by Agoerie on 2/25/18.
//  Copyright © 2018 ykdigital. All rights reserved.
//

import Foundation
import SwiftyJSON

class ListItem {
    var id: Int
    var code: String
    var idSub: Int
    var idKat: Int
    var subKategoriCode: String
    var updateByUsername: String
    var barcode: String
    var qty: Int
    var satuan: String
    var deskripsiSatuan: String
    var img: String
    var namaItem: String
    var jenisItem: String
    var kategoriItem: String
    var sellingPrice: Int
    var sellingCost: Int
    var costPercentage: Double
    var updateDate: String
    var isDelete: Bool
    var imgName: String
    var qtyGet: Int
    var note: String
    
    init(id: Int,code: String,idSub: Int,idKat: Int,subKategoriCode: String,updateByUsername: String,barcode: String,qty: Int,satuan: String,deskripsiSatuan: String,img: String,namaItem: String,jenisItem: String,kategoriItem: String,sellingPrice: Int,sellingCost: Int,costPercentage: Double,updateDate: String,isDelete: Bool,imgName: String, qtyGet: Int, note: String) {
        self.id = id
        self.code = code
        self.idSub = idSub
        self.idKat = idKat
        self.subKategoriCode = subKategoriCode
        self.updateByUsername = updateByUsername
        self.barcode = barcode
        self.qty = qty
        self.satuan = satuan
        self.deskripsiSatuan = deskripsiSatuan
        self.img = img
        self.namaItem = namaItem
        self.jenisItem = jenisItem
        self.kategoriItem = kategoriItem
        self.sellingPrice = sellingPrice
        self.sellingCost = sellingCost
        self.costPercentage = costPercentage
        self.updateDate = updateDate
        self.isDelete = isDelete
        self.imgName = imgName
        self.qtyGet = qtyGet
        self.note = note
    }
    
    convenience init(json: JSON){
        let id = json["ID_ITEM"].intValue
        let code = json["code"].stringValue
        let idSub = json["id_sub"].intValue
        let idKat = json["id_kat"].intValue
        let subKategoriCode = json["sub_kategori_code"].stringValue
        let updateByUsername = json["update_by_username"].stringValue
        let barcode = json["barcode"].stringValue
        let qty = json["qty"].intValue
        let satuan = json["satuan"].stringValue
        let deskripsiSatuan = json["deskripsi_satuan"].stringValue
        let img = json["img"].stringValue
        let namaItem = json["nama_item"].stringValue
        let jenisItem = json["jenis_item"].stringValue
        let kategoriItem = json["kategori_item"].stringValue
        let sellingPrice = json["selling_price"].intValue
        let sellingCost = json["selling_cost"].intValue
        let costPercentage = json["cost_percentage"].doubleValue
        let updateDate = json["update_date"].stringValue
        let isDelete = json["is_delete"].boolValue
        let imgName = json["img_name"].stringValue
        
        self.init(id: id, code: code, idSub: idSub, idKat: idKat, subKategoriCode: subKategoriCode, updateByUsername: updateByUsername, barcode: barcode, qty: qty, satuan: satuan, deskripsiSatuan: deskripsiSatuan, img: img, namaItem: namaItem, jenisItem: jenisItem, kategoriItem: kategoriItem, sellingPrice: sellingPrice, sellingCost: sellingCost, costPercentage: costPercentage, updateDate: updateDate, isDelete: isDelete, imgName: imgName, qtyGet: 0, note: "")
    }
    
}
