//
//  Constants.swift
//  ProjectStructure
//
//  Created by Agung Widjaja on 6/16/17.
//  Copyright © 2017 Agung. All rights reserved.
//

import Foundation

enum BuildType {
    case development
    case stagging
    case production
}

struct Config {
    static let appKey = "a5120ae232de05c4b3b54a1a8a8bfd17"
    
    /*
     *  Define URL of REST API
     */
    
    static let baseAPI: BuildType = .production
    
    static let developmentBaseAPI = "https://tpigaerp.com/tofapdev/mobile"
    static let staggingBaseAPI = "https://tpigaerp.com/tofapdev/mobile"
    static let productionBaseAPI = "https://tpigaerp.com/tofap/mobile"
    
    //"https://www.truvel.com/api/Mobile_API/"
    //"http://staging.truvel.com/api/Mobile_API/"
    
    /*
     *  Define base URL of picture
     */
    static let basePictureProdAPI = "https://tpigaerp.com/tofap/assets/img/"
    static let basePictureDevAPI = "https://tpigaerp.com/tofapdev/assets/img/"
    
    /*
     *  Define base URL of report
     */
    static let reportProdAPI = "https://tpigaerp.com/tofap"
    static let reportDevAPI = "https://tpigaerp.com/tofapdev"
}

struct REST {
    struct ServiceAPI {
        struct Menu {
            static let group = "/all_group"
        }
        
        struct User {
            static let login = "/login_karyawan"
            static let forgot = "/forget"
            static let register = "/register"
            static let editProfile = "/edit_profile"
            static let editPassword = "/ch_password"
        }
        
        struct Order {
            static let listOrder = "/ls_by_pemesan"
            static let detailOrder = "/ls_item_pemesanan"
            static let cancelOrder = "/cancel_pem"
            static let listItem = "/item"
            static let createOrder = "/trx_pemesanan/mobile"
            static let rateOrder = "/add_rate"
        }
    }
    
    struct StoryboardReferences {
        static let main = "Main"
        static let order = "Order"
        static let user = "User"
        
    }
    
    struct ViewControllerID {
        struct Menu {
            static let bar = "MenuBarViewController"
            static let floor = "FloorViewController"
            static let group = "GroupViewController"
        }
        
        struct User {
            static let login = "LoginController"
            static let forgot = "ForgotPasswordController"
            static let register = "RegisterViewController"
            static let editProfile = "EditProfileViewController"
            static let editPassword = "EditPasswordViewController"
            static let report = "ReportViewController"
        }
        
        struct Order {
            static let list = "ListOrderViewController"
            static let detail = "DetailOrderViewController"
            static let listItem = "ListItemViewController"
            static let orderSummary = "OrderSummaryViewController"
            static let successOrder = "SuccessOrderViewController"
            static let rating = "RatingViewController"
        }
    }
}
