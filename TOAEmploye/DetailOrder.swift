//
//  DetailOrder.swift
//  TOAEmploye
//
//  Created by Agoerie on 2/23/18.
//  Copyright © 2018 ykdigital. All rights reserved.
//

import Foundation
import SwiftyJSON

class DetailOrder {
    var id: Int
    var noPemesanan: String
    var idPemesan: Int
    var tglPemesanan: String
    var status: Int
    var qty: Int
    var qtyMasuk: Int
    var updateBy: Int
    var updateDate: String
    var idItPn: Int
    var note: String
    var itemName: String
    var barcode: String
    var idItem: Int
    var stock: Int
    var satuan: String
    var group: Int
    var lantai: Int
    var rate: Int
    var comment: String
    var imgName: String
    
    init(id: Int,noPemesanan: String,idPemesan: Int,tglPemesanan: String,status: Int,qty: Int,qtyMasuk: Int,updateBy: Int,updateDate: String,idItPn: Int,note: String,itemName: String,barcode: String,idItem: Int,stock: Int,satuan: String,group: Int,lantai: Int,rate: Int,comment: String,imgName: String) {
        self.id = id
        self.noPemesanan = noPemesanan
        self.idPemesan = idPemesan
        self.tglPemesanan = tglPemesanan
        self.status = status
        self.qty = qty
        self.qtyMasuk = qtyMasuk
        self.updateBy = updateBy
        self.updateDate = updateDate
        self.idItPn = idItPn
        self.note = note
        self.itemName = itemName
        self.barcode = barcode
        self.idItem = idItem
        self.stock = stock
        self.satuan = satuan
        self.group = group
        self.lantai = lantai
        self.rate = rate
        self.comment = comment
        self.imgName = imgName
    }
    
    convenience init(json: JSON){
        let id = json["id"].intValue
        let noPemesanan = json["no_pemesanan"].stringValue
        let idPemesan = json["id_pemesan"].intValue
        let tglPemesanan = json["tgl_pemesanan"].stringValue
        let status = json["status"].intValue
        let qty = json["qty"].intValue
        let qtyMasuk = json["qty_masuk"].intValue
        let updateBy = json["update_by"].intValue
        let updateDate = json["update_date"].stringValue
        let idItPn = json["id_it_pn"].intValue
        let note = json["note"].stringValue
        let itemName = json["item_name"].stringValue
        let barcode = json["barcode"].stringValue
        let idItem = json["id_item"].intValue
        let stock = json["stock"].intValue
        let satuan = json["satuan"].stringValue
        let group = json["group"].intValue
        let lantai = json["lantai"].intValue
        let rate = json["rate"].intValue
        let comment = json["comment"].stringValue
        let imgName = json["img_name"].stringValue
        
        self.init(id: id, noPemesanan: noPemesanan, idPemesan: idPemesan, tglPemesanan: tglPemesanan, status: status, qty: qty, qtyMasuk: qtyMasuk, updateBy: updateBy, updateDate: updateDate, idItPn: idItPn, note: note, itemName: itemName, barcode: barcode, idItem: idItem, stock: stock, satuan: satuan, group: group, lantai: lantai, rate: rate, comment: comment, imgName: imgName)
    }
    
}
