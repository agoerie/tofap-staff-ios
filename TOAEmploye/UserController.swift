//
//  UserController.swift
//  TOAEmploye
//
//  Created by Agoerie on 2/17/18.
//  Copyright © 2018 ykdigital. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

class UserController: BaseController {
    
    fileprivate let loginAPI = REST.ServiceAPI.User.login.ENV
    fileprivate let forgotPassAPI = REST.ServiceAPI.User.forgot.ENV
    fileprivate let registerAPI = REST.ServiceAPI.User.register.ENV
    fileprivate let editProfileAPI = REST.ServiceAPI.User.editProfile.ENV
    fileprivate let editPasswordAPI = REST.ServiceAPI.User.editPassword.ENV
    
    func postLogin(param: [String: Any], onSuccess: @escaping CollectionResultListener<String>,
                      onFailed: @escaping MessageListener,
                      onComplete: @escaping MessageListener) {
        
        let params: Parameters = param
        
        httpHelper.request(url: loginAPI, param: params, method: .post) { (success, statusCode, json) in
            if success {
                guard let data = json else {
                    onFailed("Null response from server")
                    return
                }
                
                if data.arrayValue.count > 0 {
                    if data[0]["status"].stringValue == "1" {
                        let pass = params["password"] ?? ""
                        onSuccess(200, data[0]["message"].stringValue, [data[0]["id"].stringValue,data[0]["name"].stringValue,data[0]["department"].stringValue,data[0]["jabatan"].stringValue,data[0]["email"].stringValue,data[0]["group"].stringValue,data[0]["lantai"].stringValue,data[0]["username"].stringValue,pass as! String])
                        onComplete("Fetching data completed")
                    } else {
                        onFailed(data[0]["message"].stringValue)
                    }
                }
            } else {
                if json?["error"][0]["code"].intValue == 023 {
                    onFailed("Bad request")
                } else if statusCode >= 500 {
                    onFailed("Internal server error")
                } else {
                    onFailed("An error occured")
                }
            }
        }
    }
    
    func postForgotPass(param: [String: Any], onSuccess: @escaping CollectionResultListener<String>,
                   onFailed: @escaping MessageListener,
                   onComplete: @escaping MessageListener) {
        
        let params: Parameters = param
        
        httpHelper.request(url: forgotPassAPI, param: params, method: .post) { (success, statusCode, json) in
            if success {
                guard let data = json else {
                    onFailed("Null response from server")
                    return
                }
                
                if data["status"].stringValue == "1" {
                    onSuccess(200, data["message"].stringValue, ["Success"])
                    onComplete("Fetching data completed")
                } else {
                    onFailed(data["message"].stringValue)
                }
            } else {
                if json?["error"][0]["code"].intValue == 023 {
                    onFailed("Bad request")
                } else if statusCode >= 500 {
                    onFailed("Internal server error")
                } else {
                    onFailed("An error occured")
                }
            }
        }
    }
    
    func postRegister(param: [String: Any], onSuccess: @escaping CollectionResultListener<String>,
                        onFailed: @escaping MessageListener,
                        onComplete: @escaping MessageListener) {
        
        let params: Parameters = param
        
        httpHelper.request(url: registerAPI, param: params, method: .post) { (success, statusCode, json) in
            if success {
                guard let data = json else {
                    onFailed("Null response from server")
                    return
                }
                
                if data["status"].stringValue == "1" {
                    onSuccess(200, data["message"].stringValue, ["Success"])
                    onComplete("Fetching data completed")
                } else {
                    onFailed(data["message"].stringValue)
                }
            } else {
                if json?["error"][0]["code"].intValue == 023 {
                    onFailed("Bad request")
                } else if statusCode >= 500 {
                    onFailed("Internal server error")
                } else {
                    onFailed("An error occured")
                }
            }
        }
    }
    
    func postEditProfile(param: [String: Any], onSuccess: @escaping SingleResultListener<String>,
                         onFailed: @escaping MessageListener,
                         onComplete: @escaping MessageListener) {
        
        let params: Parameters = param
        
        httpHelper.request(url: editProfileAPI, param: params, method: .post) { (success, statusCode, json) in
            if success {
                guard let data = json else {
                    onFailed("Null response from server")
                    return
                }
                
                if data["status"].intValue == 1 {
                    onSuccess(200, "Success fetching data", data["message"].stringValue)
                    onComplete("Fetching data completed")
                } else {
                    onFailed(data["message"].stringValue)
                }
            } else {
                if json?["error"][0]["code"].intValue == 023 {
                    onFailed("Bad request")
                } else if statusCode >= 500 {
                    onFailed("Internal server error")
                } else {
                    onFailed("An error occured")
                }
            }
        }
    }
    
    func postEditPassword(param: [String: Any], onSuccess: @escaping SingleResultListener<String>,
                         onFailed: @escaping MessageListener,
                         onComplete: @escaping MessageListener) {
        
        let params: Parameters = param
        
        httpHelper.request(url: editPasswordAPI, param: params, method: .post) { (success, statusCode, json) in
            if success {
                guard let data = json else {
                    onFailed("Null response from server")
                    return
                }
                
                if data["status"].intValue == 1 {
                    onSuccess(200, "Success fetching data", data["message"].stringValue)
                    onComplete("Fetching data completed")
                } else {
                    onFailed(data["message"].stringValue)
                }
            } else {
                if json?["error"][0]["code"].intValue == 023 {
                    onFailed("Bad request")
                } else if statusCode >= 500 {
                    onFailed("Internal server error")
                } else {
                    onFailed("An error occured")
                }
            }
        }
    }
    
}
