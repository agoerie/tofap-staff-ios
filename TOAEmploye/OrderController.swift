//
//  OrderController.swift
//  TOAEmploye
//
//  Created by Agoerie on 2/20/18.
//  Copyright © 2018 ykdigital. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

class OrderController: BaseController {
    
    fileprivate let listOrderAPI = REST.ServiceAPI.Order.listOrder.ENV
    fileprivate let detailOrderAPI = REST.ServiceAPI.Order.detailOrder.ENV
    fileprivate let cancelOrderAPI = REST.ServiceAPI.Order.cancelOrder.ENV
    fileprivate let listItemAPI = REST.ServiceAPI.Order.listItem.ENV
    fileprivate let createOrderAPI = REST.ServiceAPI.Order.createOrder.ENV
    fileprivate let rateOrderAPI = REST.ServiceAPI.Order.rateOrder.ENV
    
    func postListOrder(param: [String: Any], onSuccess: @escaping CollectionResultListener<ListOrder>,
                   onFailed: @escaping MessageListener,
                   onComplete: @escaping MessageListener) {
        
        let params: Parameters = param
        
        httpHelper.request(url: listOrderAPI, param: params, method: .post) { (success, statusCode, json) in
            if success {
                guard let data = json else {
                    onFailed("Null response from server")
                    return
                }
                
                if data.arrayValue.count > 0 {
                    var listOrder = [ListOrder]()
                    for value in data.arrayValue {
                        let listOrd = ListOrder(json: value)
                        listOrder.append(listOrd)
                    }
                    
                    onSuccess(200, "Success fetching data", listOrder)
                    onComplete("Fetching data completed")
                } else {
                    onFailed("An error occured")
                }
            } else {
                if json?["error"][0]["code"].intValue == 023 {
                    onFailed("Bad request")
                } else if statusCode >= 500 {
                    onFailed("Internal server error")
                } else {
                    onFailed("An error occured")
                }
            }
        }
    }
    
    func getDetailOrder(id: String, onSuccess: @escaping CollectionResultListener<DetailOrder>,
                       onFailed: @escaping MessageListener,
                       onComplete: @escaping MessageListener) {
        
        httpHelper.request(url: detailOrderAPI + "/\(id)", param: nil, method: .get) { (success, statusCode, json) in
            if success {
                guard let data = json else {
                    onFailed("Null response from server")
                    return
                }
                
                if data.arrayValue.count > 0 {
                    var detailOrder = [DetailOrder]()
                    for value in data.arrayValue {
                        let detailOrd = DetailOrder(json: value)
                        detailOrder.append(detailOrd)
                    }
                    
                    onSuccess(200, "Success fetching data", detailOrder)
                    onComplete("Fetching data completed")
                } else {
                    onFailed("An error occured")
                }
            } else {
                if json?["error"][0]["code"].intValue == 023 {
                    onFailed("Bad request")
                } else if statusCode >= 500 {
                    onFailed("Internal server error")
                } else {
                    onFailed("An error occured")
                }
            }
        }
    }
    
    func postCancelOrder(param: [String: Any], onSuccess: @escaping SingleResultListener<String>,
                       onFailed: @escaping MessageListener,
                       onComplete: @escaping MessageListener) {
        
        let params: Parameters = param
        
        httpHelper.request(url: cancelOrderAPI, param: params, method: .post) { (success, statusCode, json) in
            if success {
                guard let data = json else {
                    onFailed("Null response from server")
                    return
                }
                
                if data["status"].intValue == 1 {
                    onSuccess(200, "Success fetching data", data["message"].stringValue)
                    onComplete("Fetching data completed")
                } else {
                    onFailed(data["message"].stringValue)
                }
            } else {
                if json?["error"][0]["code"].intValue == 023 {
                    onFailed("Bad request")
                } else if statusCode >= 500 {
                    onFailed("Internal server error")
                } else {
                    onFailed("An error occured")
                }
            }
        }
    }
    
    func getListItem(onSuccess: @escaping CollectionResultListener<ListItem>,
                        onFailed: @escaping MessageListener,
                        onComplete: @escaping MessageListener) {
        
        httpHelper.request(url: listItemAPI, param: nil, method: .get) { (success, statusCode, json) in
            if success {
                guard let data = json else {
                    onFailed("Null response from server")
                    return
                }
                
                if data.arrayValue.count > 0 {
                    var listItems = [ListItem]()
                    for value in data.arrayValue {
                        let listItem = ListItem(json: value)
                        listItems.append(listItem)
                    }
                    
                    onSuccess(200, "Success fetching data", listItems)
                    onComplete("Fetching data completed")
                } else {
                    onFailed("An error occured")
                }
            } else {
                if json?["error"][0]["code"].intValue == 023 {
                    onFailed("Bad request")
                } else if statusCode >= 500 {
                    onFailed("Internal server error")
                } else {
                    onFailed("An error occured")
                }
            }
        }
    }
    
    func postCreateOrder(param: [String: Any], onSuccess: @escaping SingleResultListener<String>,
                         onFailed: @escaping MessageListener,
                         onComplete: @escaping MessageListener) {
        
        let params: Parameters = param
        
        httpHelper.request(url: createOrderAPI, param: params, method: .post) { (success, statusCode, json) in
            if success {
                guard let data = json else {
                    onFailed("Null response from server")
                    return
                }
                
                if data["status"].intValue == 1 {
                    onSuccess(200, "Success fetching data", data["message"].stringValue)
                    onComplete("Fetching data completed")
                } else {
                    onFailed(data["message"].stringValue)
                }
            } else {
                if json?["error"][0]["code"].intValue == 023 {
                    onFailed("Bad request")
                } else if statusCode >= 500 {
                    onFailed("Internal server error")
                } else {
                    onFailed("An error occured")
                }
            }
        }
    }
    
    func postRating(param: [String: Any], onSuccess: @escaping SingleResultListener<String>,
                       onFailed: @escaping MessageListener,
                       onComplete: @escaping MessageListener) {
        
        let params: Parameters = param
        
        httpHelper.request(url: rateOrderAPI, param: params, method: .post) { (success, statusCode, json) in
            if success {
                guard let data = json else {
                    onFailed("Null response from server")
                    return
                }
                print(data)
                if data[0]["status"].intValue == 1 {
                    onSuccess(200, "Success fetching data", data[0]["message"].stringValue)
                    onComplete("Fetching data completed")
                } else {
                    onFailed(data[0]["message"].stringValue)
                }
            } else {
                if json?["error"][0]["code"].intValue == 023 {
                    onFailed("Bad request")
                } else if statusCode >= 500 {
                    onFailed("Internal server error")
                } else {
                    onFailed("An error occured")
                }
            }
        }
    }
    
}
