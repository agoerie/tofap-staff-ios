//
//  String+Extension.swift
//  ProjectStructure
//
//  Created by Agung Widjaja on 6/7/17.
//  Copyright © 2017 Agung. All rights reserved.
//

import Foundation

extension String {
    var basePictureURL: String {
        switch Config.baseAPI {
        case .development:
            return Config.basePictureDevAPI + self
        case .stagging:
            return Config.basePictureDevAPI + self
        default:
            return Config.basePictureProdAPI + self
        }
    }
    
    var reportURL: String {
        switch Config.baseAPI {
        case .development:
            return Config.reportDevAPI + self
        case .stagging:
            return Config.reportDevAPI + self
        default:
            return Config.reportProdAPI + self
        }
    }
    
    var ENV: String {
        switch Config.baseAPI {
        case .development:
            return Config.developmentBaseAPI + self
        case .stagging:
            return Config.staggingBaseAPI + self
        case .production:
            return Config.productionBaseAPI + self
        }
    }
}
