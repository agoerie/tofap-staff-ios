//
//  Checkbox.swift
//  Truvel
//
//  Created by Agung Widjaja on 10/19/17.
//  Copyright © 2017 Agung. All rights reserved.
//

import Foundation
import UIKit

/**
 CheckboxDelegate protocol to be confirmed
 when you need callbacks for checkboc status change
 **/
@objc protocol CheckboxDelegate {
    func didSelect(_ checkbox: Checkbox)
    func didDeselect(_ checkbox: Checkbox)
}

/**
 CCheckbox is a custom UIButton class
 which represents custom checkbox with
 custom images
 **/

@IBDesignable class Checkbox: UIButton {
    
    //MARK:- Variables
    @IBInspectable var normalImage: UIImage? = #imageLiteral(resourceName: "ic-square")
    @IBInspectable var selectedImage: UIImage? = #imageLiteral(resourceName: "ic-square-select")
    
    /// when set this variable checkbox status changed
    @IBInspectable var isCheckboxSelected: Bool = false {
        didSet {
            self.imageView?.contentMode = .scaleAspectFit
            self.changeStatus()
        }
    }
    
    open weak var delegate: CheckboxDelegate?
    var animation: UIViewAnimationOptions = .transitionCrossDissolve
    
    //MARK:- Customize Button Properties
    override func layoutSubviews() {
        super.layoutSubviews()
        initComponent()
    }
    
    private func initComponent () {
        //Check Button Type Value
        if self.buttonType != .system
            && self.buttonType != .custom {
            fatalError("Button Type Error. Please Make Button Type System or Custom")
        }
        self.setTitle("", for: .normal)
        self.addTarget(self,
                       action: #selector(Checkbox.didTouchUpInside(_:)),
                       for: .touchUpInside)
        
        //Styling UIView
        self.backgroundColor = UIColor.clear
    }
    
    //MARK:- Checkbox Status
    private func changeStatus() {
        if isCheckboxSelected {
            UIView.transition(with: self.imageView!,
                              duration:0.5,
                              options: animation,
                              animations: { self.setImage(self.selectedImage, for: .normal) },
                              completion: nil)
            
        } else {
            UIView.transition(with: self.imageView!,
                              duration:0.5,
                              options: animation,
                              animations: { self.setImage(self.normalImage, for: .normal) },
                              completion: nil)
        }
        
    }
    
    // MARK: IBActions
    @objc func didTouchUpInside(_ sender: UIButton) {
        self.isCheckboxSelected = !self.isCheckboxSelected
        
        if isCheckboxSelected && delegate != nil {
            delegate?.didSelect(self)
        } else if delegate != nil {
            delegate?.didDeselect(self)
        }
    }
}
