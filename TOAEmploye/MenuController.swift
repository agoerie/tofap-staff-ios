//
//  MenuController.swift
//  TOAEmploye
//
//  Created by Agoerie on 3/3/18.
//  Copyright © 2018 ykdigital. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

class MenuController: BaseController {
    
    fileprivate let groupAPI = REST.ServiceAPI.Menu.group.ENV
    
    func getListGroup(onSuccess: @escaping CollectionResultListener<ListGroup>,
                   onFailed: @escaping MessageListener,
                   onComplete: @escaping MessageListener) {
        
        httpHelper.request(url: groupAPI, param: nil, method: .get) { (success, statusCode, json) in
            if success {
                guard let data = json else {
                    onFailed("Null response from server")
                    return
                }
                
                if data.arrayValue.count > 0 {
                    var listGroups = [ListGroup]()
                    for value in data.arrayValue {
                        let listGroup = ListGroup(json: value)
                        listGroups.append(listGroup)
                    }
                    
                    onSuccess(200, "Success fetching data", listGroups)
                    onComplete("Fetching data completed")
                } else {
                    onFailed("An error occured")
                }
            } else {
                if json?["error"][0]["code"].intValue == 023 {
                    onFailed("Bad request")
                } else if statusCode >= 500 {
                    onFailed("Internal server error")
                } else {
                    onFailed("An error occured")
                }
            }
        }
    }
    
}
