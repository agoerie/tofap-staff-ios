//
//  ListOrderViewController.swift
//  TOAEmploye
//
//  Created by Agoerie on 2/19/18.
//  Copyright © 2018 ykdigital. All rights reserved.
//

import UIKit
import SwiftyJSON
import DGActivityIndicatorView

class ListOrderViewController: UIViewController {
    
    @IBOutlet weak var table: UITableView!{
        didSet{
            let xib = ListOrderCell.nib
            table.register(xib, forCellReuseIdentifier: ListOrderCell.identifier)
            
            table.delegate = self
            table.dataSource = self
            
            table.separatorStyle = .none
            table.tableFooterView = UIView()
        }
    }
    @IBOutlet var lblSearch: UILabel!
    @IBOutlet var indicatorView: DGActivityIndicatorView!
    @IBOutlet var constraintLblSearch: NSLayoutConstraint!
    @IBOutlet var imgMenu: UIImageView!
    @IBOutlet var lblCourier: UILabel!
    @IBOutlet var btnOrder: UIButton!
    
    var refreshControl: UIRefreshControl!
    
    var listOrder = [ListOrder]() {
        didSet {
            temps = listOrder
            table.reloadData()
        }
    }
    
    var temps :[ListOrder] = []
    var idLogin: String = "0"
    var username: String = ""
    var pass: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(setupView), name: Notification.Name("reloadDataListOrder"), object: nil)
        
        setupView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @objc func setupView() {
        self.table.isHidden = true
        lblSearch.isHidden = false
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(getDataOrder), for: .valueChanged)
        
        table.addSubview(refreshControl)
        
        indicatorView.type = DGActivityIndicatorAnimationType.ballPulse
        indicatorView.tintColor = UIColor(hexString: "00C200")
        indicatorView.startAnimating()
        
        if let userData = UserDefaults.standard.string(forKey: "userdata") {
            let jsonArray = JSON.init(parseJSON: userData).arrayValue
            if jsonArray.count < 9 {
                logout()
                return
            }
            
            lblCourier.text = jsonArray[1].stringValue
            idLogin = jsonArray[0].stringValue
            username = jsonArray[7].stringValue
            pass = jsonArray[8].stringValue
        }
        
        btnOrder.isEnabled = false
        btnOrder.backgroundColor = UIColor.init(hexString: "25D440").withAlphaComponent(0.5)
        
        getDataOrder()
    }

    @IBAction func menuAction(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: REST.StoryboardReferences.main, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.Menu.bar) as! MenuBarViewController
        vc.delegate = self
        
        self.addChildViewController(vc)
        vc.didMove(toParentViewController: vc)
        vc.view.frame = UIScreen.main.bounds
        vc.view.layoutIfNeeded()
        vc.view.backgroundColor = UIColor.clear
        self.view.addSubview(vc.view)
        self.view.bringSubview(toFront: vc.view)
    }
    
    @IBAction func orderNowAction(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: REST.StoryboardReferences.order, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.Order.listItem) as! ListItemViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func getDataOrder() {
        OrderController().postListOrder(param: ["id":idLogin], onSuccess: { (code, message, results) in
            guard let res = results else { return }
            
            let sortedArray = res.sorted {
                $0.tglPemesanan > $1.tglPemesanan
            }
            
            self.listOrder = sortedArray
            
            self.checkLastOrder(data: res)
        }, onFailed: { (message) in
            print(message)
            print("Do action when data failed to fetching here")
            self.lblSearch.isHidden = true
            self.indicatorView.stopAnimating()
            self.refreshControl.endRefreshing()
        }) { (message) in
            print(message)
            print("Do action when data complete to fetching here")
            
            self.lblSearch.isHidden = true
            self.constraintLblSearch.constant = 0
            self.indicatorView.stopAnimating()
            self.refreshControl.endRefreshing()
            self.table.isHidden = false
        }
    }
    
    func postRating(data: ListOrder, rating: Int) {
        self.indicatorView.startAnimating()
        
        let param = ["id_kurir":data.idKurir,"id_pemesan":data.idPemesan,"id_pemesanan":data.idPemesanan,"rate":rating]
        OrderController().postRating(param: param, onSuccess: { (code, message, result) in
            self.indicatorView.stopAnimating()
        }, onFailed: { (message) in
            print(message)
            print("Do action when data failed to fetching here")
            
            self.indicatorView.stopAnimating()
            
            self.getDataOrder()
        }) { (message) in
            print(message)
            print("Do action when data complete to fetching here")
            
            self.getDataOrder()
        }
    }
    
    func checkLastOrder(data: [ListOrder]) {
        let filterArray = data.filter {
            $0.txtStat == "Done"
        }.filter {
            $0.rate == 0
        }
        
        if filterArray.count > 0 {
            // to rating screen
            toRatingPage(data: filterArray[0])
            
            return
        }
        
        let chkCancelStat = data.filter {
            $0.txtStat != "Cancel"
        }.filter {
            $0.txtStat != "Done"
        }
        
        if chkCancelStat.count > 0 {
            btnOrder.isEnabled = false
            btnOrder.backgroundColor = UIColor.init(hexString: "25D440").withAlphaComponent(0.5)
        } else {
            btnOrder.isEnabled = true
            btnOrder.backgroundColor = UIColor.init(hexString: "25D440").withAlphaComponent(1)
        }
    }
    
    func toRatingPage(data: ListOrder) {
        let storyboard = UIStoryboard(name: REST.StoryboardReferences.order, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.Order.rating) as! RatingViewController
        vc.dataOrder = data
        vc.delegate = self
        
        self.addChildViewController(vc)
        vc.didMove(toParentViewController: vc)
        vc.view.frame = UIScreen.main.bounds
        vc.view.layoutIfNeeded()
        vc.view.backgroundColor = UIColor.clear
        self.view.addSubview(vc.view)
        self.view.bringSubview(toFront: vc.view)
    }
    
    func toDetailOrder(data: ListOrder) {
        let storyboard = UIStoryboard(name: REST.StoryboardReferences.order, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.Order.detail) as! DetailOrderViewController
        vc.listOrder = data
        vc.delegate = self
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func toReportPage() {
        if username.characters.count < 2 || pass.characters.count < 2 {
            return
        }
        
        let storyboard = UIStoryboard(name: REST.StoryboardReferences.user, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.User.report) as! ReportViewController
        vc.link = "/report/log_by_karyawan?by=0&bulan=0&tahun=0&group=0&type=employee&mobile=1&username=\(username)&password=\(pass)".reportURL
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func toEditProfile() {
        let storyboard = UIStoryboard(name: REST.StoryboardReferences.user, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.User.editProfile) as! EditProfileViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func logout() {
        if UserDefaults().contains(key: "userdata") {
            UserDefaults.standard.removeObject(forKey: "userdata")
            toLoginScreen()
        }
    }
    
    func toLoginScreen() {
        let storyboard = UIStoryboard(name: REST.StoryboardReferences.main, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.User.login) as! LoginController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = vc
        appDelegate.window?.makeKeyAndVisible()
    }
}

extension ListOrderViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listOrder.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = listOrder[indexPath.row]
        
        return ListOrderCell.configure(context: self, tableView: tableView, indexPath: indexPath, object: data)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = listOrder[indexPath.row]
        
        toDetailOrder(data: data)
    }
}

extension ListOrderViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
}

extension ListOrderViewController: detailOrderDelegate {
    
    func cancelOrder(controller: DetailOrderViewController) {
        getDataOrder()
        
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension ListOrderViewController: menuViewDelegate {
    
    func onDismiss(controller: MenuBarViewController) {
        controller.willMove(toParentViewController: nil)
        controller.view.removeFromSuperview()
    }
    
    func onSelectMenu(controller: MenuBarViewController, menu: String) {
        controller.willMove(toParentViewController: nil)
        controller.view.removeFromSuperview()
        
        switch menu {
        case "Profile":
            toEditProfile()
        case "Logout":
            logout()
        default:
            toReportPage()
        }
    }
    
}

extension ListOrderViewController: ratingDelegate {
    func pressSubmit(controller: RatingViewController, data: ListOrder, rating: Int, comment: String) {
        controller.willMove(toParentViewController: nil)
        controller.view.removeFromSuperview()
        
        postRating(data: data, rating: rating)
    }
}
