//
//  ForgotPasswordController.swift
//  TOAEmploye
//
//  Created by Agoerie on 2/16/18.
//  Copyright © 2018 ykdigital. All rights reserved.
//

import UIKit
import AnimatedTextInput

class ForgotPasswordController: UIViewController {

    @IBOutlet var forgotBtn: UIButton!
    @IBOutlet var usernameTF: AnimatedTextInput!
    @IBOutlet var emailTF: AnimatedTextInput!
    @IBOutlet var closeImg: UIImageView!
    
    var activityIndicator: UIActivityIndicatorView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setupView() {
        let gesture = UITapGestureRecognizer.init(target: self, action: #selector(resignResponder))
        view.addGestureRecognizer(gesture)
        
        forgotBtn.layer.cornerRadius = 22.5
        
        usernameTF.placeHolderText = "Username"
        usernameTF.type = .standard
        usernameTF.style = CustomTextInputStyle() as AnimatedTextInputStyle
        
        emailTF.placeHolderText = "Email"
        emailTF.type = .email
        emailTF.style = CustomTextInputStyle() as AnimatedTextInputStyle
        
        closeImg.image = UIImage.init(named: "ic-close")?.withRenderingMode(.alwaysTemplate)
        closeImg.tintColor = UIColor.white
        
        activityIndicator = UIActivityIndicatorView.init(activityIndicatorStyle: .whiteLarge)
        activityIndicator?.center = view.center
        activityIndicator?.hidesWhenStopped = true
        activityIndicator?.stopAnimating()
        
        view.addSubview(activityIndicator!)
    }

    @IBAction func forgotAction(_ sender: UIButton) {
        activityIndicator?.startAnimating()
        forgotBtn.isEnabled = false
        
        let param = ["username":usernameTF.text!,"email":emailTF.text!]
        
        UserController().postForgotPass(param: param, onSuccess: { (code, message, results) in
            print(message)
            print("Do action when data success to fetching here")
            self.activityIndicator?.stopAnimating()
            self.forgotBtn.isEnabled = true
            
            let alert = UIAlertController.init(title: "Sukses", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: nil))
            alert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: { (alert) in
                self.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }, onFailed: { (message) in
            print(message)
            print("Do action when data failed to fetching here")
            self.activityIndicator?.stopAnimating()
            self.forgotBtn.isEnabled = true
            
            let alert = UIAlertController.init(title: nil, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }) { (message) in
            print(message)
            print("Do action when data complete to fetching here")
        }
    }
    
    @IBAction func closeAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func resignResponder() {
        self.view.endEditing(true)
    }
    
}

struct CustomTextInputStyle: AnimatedTextInputStyle {
    let placeholderInactiveColor = UIColor.white
    let activeColor = UIColor.white
    let inactiveColor = UIColor.white.withAlphaComponent(1.0)
    let lineInactiveColor = UIColor.white.withAlphaComponent(1.0)
    let lineActiveColor = UIColor.white.withAlphaComponent(1.0)
    let lineHeight: CGFloat = 1
    let errorColor = UIColor.red
    let textInputFont = UIFont.systemFont(ofSize: 17)
    let textInputFontColor = UIColor.white
    let placeholderMinFontSize: CGFloat = 9
    let counterLabelFont: UIFont? = UIFont.systemFont(ofSize: 9)
    let leftMargin: CGFloat = 0
    let topMargin: CGFloat = 15
    let rightMargin: CGFloat = 0
    let bottomMargin: CGFloat = 0
    let yHintPositionOffset: CGFloat = 7
    let yPlaceholderPositionOffset: CGFloat = 0
    public let textAttributes: [String: Any]? = nil
}
