//
//  EditPasswordViewController.swift
//  TOAEmploye
//
//  Created by Agoerie on 2/28/18.
//  Copyright © 2018 ykdigital. All rights reserved.
//

import UIKit
import SwiftyJSON
import AnimatedTextInput

class EditPasswordViewController: UIViewController {
    
    @IBOutlet var lblOldPassword: AnimatedTextInput!
    @IBOutlet var lblNewPassword: AnimatedTextInput!
    @IBOutlet var lblConfirmPassword: AnimatedTextInput!
    @IBOutlet var btnSave: UIButton!
    @IBOutlet var scrollView: UIScrollView!
    
    var idLogin: String = "0"

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func setupView() {
        let gesture = UITapGestureRecognizer.init(target: self, action: #selector(resignResponder))
        view.addGestureRecognizer(gesture)
        
        btnSave.layer.cornerRadius = 22.5
        
        lblOldPassword.placeHolderText = "Old Password"
        lblOldPassword.type = .password
        lblOldPassword.style = CustomTextInputStyle() as AnimatedTextInputStyle
        
        lblNewPassword.placeHolderText = "New Password"
        lblNewPassword.type = .password
        lblNewPassword.style = CustomTextInputStyle() as AnimatedTextInputStyle
        
        lblConfirmPassword.placeHolderText = "Confirm Password"
        lblConfirmPassword.type = .password
        lblConfirmPassword.style = CustomTextInputStyle() as AnimatedTextInputStyle
        
        if let userData = UserDefaults.standard.string(forKey: "userdata") {
            let jsonArray = JSON.parse(userData).arrayValue
            idLogin = jsonArray[0].stringValue
        }
    }

    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveAction(_ sender: UIButton) {
        if lblNewPassword.text! != lblConfirmPassword.text! {
            let alert = JDropDownAlert(position: .top)
            alert.alertWith("Password tidak sesuai dengan password konfirmasi.", delay: 2)
            
            return
        }
        
        let param: [String: Any] = ["id_user":idLogin,"old":lblOldPassword.text!,"new":lblNewPassword.text!]
        
        UserController().postEditPassword(param: param, onSuccess: { (code, message, result) in
            let alert = JDropDownAlert(position: .top)
            alert.alertWith("Sukses", message: result, topLabelColor: UIColor.white, messageLabelColor: UIColor.white, backgroundColor: UIColor.init(hexString: "25D440"), delay: 2)
        }, onFailed: { (message) in
            print(message)
            print("Do action when data failed to fetching here")
            let alert = JDropDownAlert(position: .top)
            alert.alertWith(message, delay: 2)
        }) { (message) in
            print(message)
            print("Do action when data complete to fetching here")
        }
    }
    
    @IBAction func toEditAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func resignResponder() {
        self.view.endEditing(true)
    }
    
}
