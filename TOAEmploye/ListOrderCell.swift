//
//  ListOrderCell.swift
//  TOAEmploye
//
//  Created by Agoerie on 2/19/18.
//  Copyright © 2018 ykdigital. All rights reserved.
//

import UIKit
import HCSStarRatingView

class ListOrderCell: UITableViewCell {

    
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblQty: UILabel!
    @IBOutlet var lblCourier: UILabel!
    @IBOutlet var lblStatus: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var hcsRating: HCSStarRatingView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        super.selectionStyle = .none
    }

}

extension ListOrderCell: TableViewCellProtocol {
    
    static func configure<T>(context: UIViewController, tableView: UITableView, indexPath: IndexPath, object: T) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ListOrderCell.identifier, for: indexPath) as! ListOrderCell
        
        guard let data = object as? ListOrder else { return cell }
        
        cell.lblTitle.text = data.itemList
        cell.lblQty.text = "Total Items : \(data.totalBarang)"
        cell.lblCourier.text = data.kurir
        
        cell.lblStatus.text = data.txtStat
        cell.lblDate.text = data.tglPemesanan
        
        cell.hcsRating.value = CGFloat(data.rate)
        cell.hcsRating.starBorderColor = UIColor.lightGray
        cell.hcsRating.tintColor = UIColor.init(hexString: "FFFF00")
        
        return cell
    }
    
}
