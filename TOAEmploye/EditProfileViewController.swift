//
//  EditProfileViewController.swift
//  TOAEmploye
//
//  Created by Agoerie on 2/28/18.
//  Copyright © 2018 ykdigital. All rights reserved.
//

import UIKit
import SwiftyJSON
import AnimatedTextInput

class EditProfileViewController: UIViewController {

    @IBOutlet var lblName: AnimatedTextInput!
    @IBOutlet var lblEmail: AnimatedTextInput!
    @IBOutlet var lblGroup: AnimatedTextInput!
    @IBOutlet var lblFloor: AnimatedTextInput!
    @IBOutlet var btnSave: UIButton!
    
    @IBOutlet var scrollView: UIScrollView!
    
    let data = ["First Floor","Second Floor","Third Floor","Fourth Floor","Fifth Floor"]
    var idLogin: String = "0"
    var floor: String = "1"
    var idGroup: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func setupView() {
        let gesture = UITapGestureRecognizer.init(target: self, action: #selector(resignResponder))
        view.addGestureRecognizer(gesture)
        gesture.cancelsTouchesInView = false
        
        btnSave.layer.cornerRadius = 22.5
        
        lblName.placeHolderText = "Name"
        lblName.type = .standard
        lblName.style = CustomTextInputStyle() as AnimatedTextInputStyle
        
        lblEmail.placeHolderText = "Email"
        lblEmail.type = .email
        lblEmail.style = CustomTextInputStyle() as AnimatedTextInputStyle
        
        lblGroup.placeHolderText = "Group"
        lblGroup.type = .standard
        lblGroup.tag = 1
        lblGroup.delegate = self
        lblGroup.style = CustomTextInputStyle() as AnimatedTextInputStyle
        
        lblFloor.placeHolderText = "Floor"
        lblFloor.type = .numeric
        lblFloor.tag = 2
        lblFloor.delegate = self
        lblFloor.style = CustomTextInputStyle() as AnimatedTextInputStyle
        
        if let userData = UserDefaults.standard.string(forKey: "userdata") {
            let jsonArray = JSON.parse(userData).arrayValue
            idLogin = jsonArray[0].stringValue
            floor = jsonArray[6].stringValue
            
            lblName.text = jsonArray[1].stringValue
            lblEmail.text = jsonArray[4].stringValue
            lblGroup.text = jsonArray[5].stringValue
            
            if Int(floor)! != 0 {
                lblFloor.text = data[Int(floor)! - 1]
            }
            
            getDataGroup()
        }
    }

    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveAction(_ sender: UIButton) {
        let param: [String: Any] = ["id":idLogin,"name":lblName.text!,"jabatan":"","department":"","email":lblEmail.text!,"group":"\(idGroup)","lantai":floor]
        
        UserController().postEditProfile(param: param, onSuccess: { (code, message, result) in
            let alert = JDropDownAlert(position: .top)
            alert.alertWith("Sukses", message: result, topLabelColor: UIColor.white, messageLabelColor: UIColor.white, backgroundColor: UIColor.init(hexString: "25D440"), delay: 2)
        }, onFailed: { (message) in
            print(message)
            print("Do action when data failed to fetching here")
            let alert = JDropDownAlert(position: .top)
            alert.alertWith(message, delay: 2)
        }) { (message) in
            print(message)
            print("Do action when data complete to fetching here")
            
            if let userData = UserDefaults.standard.string(forKey: "userdata") {
                let jsonArray = JSON.parse(userData).arrayValue
                let userSession = [jsonArray[0].stringValue,jsonArray[1].stringValue,jsonArray[2].stringValue,jsonArray[3].stringValue,self.lblEmail.text!,self.lblGroup.text!,self.floor,jsonArray[7].stringValue,jsonArray[8].stringValue]
                
                let jsonString = JSON(userSession).rawString()
                UserDefaults.standard.set(jsonString, forKey: "userdata")
            }
        }
    }
    
    @IBAction func changeAction(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: REST.StoryboardReferences.user, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.User.editPassword) as! EditPasswordViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func resignResponder() {
        self.view.endEditing(true)
    }
    
    func selectFloor() {
        let storyboard = UIStoryboard(name: REST.StoryboardReferences.main, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.Menu.floor) as! FloorViewController
        vc.delegate = self
        
        self.addChildViewController(vc)
        vc.didMove(toParentViewController: vc)
        vc.view.frame = UIScreen.main.bounds
        vc.view.layoutIfNeeded()
        vc.view.backgroundColor = UIColor.clear
        self.view.addSubview(vc.view)
        self.view.bringSubview(toFront: vc.view)
    }
    
    func selectGroup() {
        let storyboard = UIStoryboard(name: REST.StoryboardReferences.main, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.Menu.group) as! GroupViewController
        vc.delegate = self
        
        self.addChildViewController(vc)
        vc.didMove(toParentViewController: vc)
        vc.view.frame = UIScreen.main.bounds
        vc.view.layoutIfNeeded()
        vc.view.backgroundColor = UIColor.clear
        self.view.addSubview(vc.view)
        self.view.bringSubview(toFront: vc.view)
    }
    
    func getDataGroup() {
        MenuController().getListGroup(onSuccess: { (code, message, results) in
            guard let res = results else { return }
            
            let filterArray = res.filter {
                $0.groupName == self.lblGroup.text
            }
            
            if filterArray.count > 0 {
                self.idGroup = filterArray[0].id
            }
        }, onFailed: { (message) in
            print(message)
            print("Do action when data failed to fetching here")
        }) { (message) in
            print(message)
            print("Do action when data complete to fetching here")
        }
    }
    
}

extension EditProfileViewController: AnimatedTextInputDelegate {
    func animatedTextInputShouldBeginEditing(animatedTextInput: AnimatedTextInput) -> Bool {
        if animatedTextInput.tag == 1 {
            selectGroup()
        }
        
        if animatedTextInput.tag == 2 {
            selectFloor()
        }
        
        return false
    }
}

extension EditProfileViewController: floorViewDelegate {
    func onDismiss(controller: FloorViewController) {
        controller.willMove(toParentViewController: nil)
        controller.view.removeFromSuperview()
    }
    
    func onSelectMenu(controller: FloorViewController, menu: String, id: Int) {
        controller.willMove(toParentViewController: nil)
        controller.view.removeFromSuperview()
        
        print(data.index(of: menu)!)
        floor = "\(data.index(of: menu)! + 1)"
        lblFloor.text = menu
    }
}

extension EditProfileViewController: groupViewDelegate {
    func onDismissGroup(controller: GroupViewController) {
        controller.willMove(toParentViewController: nil)
        controller.view.removeFromSuperview()
    }
    
    func onSelectMenuGroup(controller: GroupViewController, menu: String, id: Int) {
        controller.willMove(toParentViewController: nil)
        controller.view.removeFromSuperview()
        
        lblGroup.text = menu
        idGroup = id
    }
}
