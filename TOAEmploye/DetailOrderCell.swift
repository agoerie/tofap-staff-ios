//
//  DetailOrderCell.swift
//  TOAEmploye
//
//  Created by Agoerie on 2/23/18.
//  Copyright © 2018 ykdigital. All rights reserved.
//

import UIKit
import SDWebImage

class DetailOrderCell: UITableViewCell {

    @IBOutlet var imgProduct: UIImageView!
    @IBOutlet var lblProdName: UILabel!
    @IBOutlet var lblQty: UILabel!
    @IBOutlet var viewContainer: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        super.selectionStyle = .none
    }

}

extension DetailOrderCell: TableViewCellProtocol {
    
    static func configure<T>(context: UIViewController, tableView: UITableView, indexPath: IndexPath, object: T) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: DetailOrderCell.identifier, for: indexPath) as! DetailOrderCell
        
        guard let data = object as? DetailOrder else { return cell }
        
        cell.lblProdName.text = data.itemName
        cell.lblQty.text = "Quantity : \(data.qty)"
        
        cell.imgProduct.clipsToBounds = true
        cell.imgProduct.sd_setImage(with: URL.init(string: "\(data.imgName.basePictureURL)"), placeholderImage: UIImage.init(named: "no-image.png"))
        
        cell.viewContainer.layer.cornerRadius = 2.5
        
        return cell
    }
    
}
