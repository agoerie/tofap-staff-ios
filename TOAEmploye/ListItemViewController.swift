//
//  ListItemViewController.swift
//  TOAEmploye
//
//  Created by Agoerie on 2/24/18.
//  Copyright © 2018 ykdigital. All rights reserved.
//

import UIKit
import AnimatedTextInput
import DGActivityIndicatorView

class ListItemViewController: UIViewController {
    
    @IBOutlet weak var table: UITableView!{
        didSet{
            let xib = ListItemCell.nib
            table.register(xib, forCellReuseIdentifier: ListItemCell.identifier)
            
            table.delegate = self
            table.dataSource = self
            
            table.separatorStyle = .none
            table.tableFooterView = UIView()
        }
    }
    @IBOutlet var lblSearch: UILabel!
    @IBOutlet var indicatorView: DGActivityIndicatorView!
    @IBOutlet var constraintLblSearch: NSLayoutConstraint!
    @IBOutlet var viewSearchContainer: UIView!
    @IBOutlet var tfSearch: UITextField!
    
    var listItem = [ListItem]() {
        didSet {
            if temps.count == 0 {
                temps = listItem
            }
            
            table.reloadData()
        }
    }
    
    var temps :[ListItem] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setupView() {
        self.table.isHidden = true
        lblSearch.isHidden = false
        
        tfSearch.clearButtonMode = .whileEditing
        
        indicatorView.type = DGActivityIndicatorAnimationType.ballPulse
        indicatorView.tintColor = UIColor(hexString: "00C200")
        indicatorView.startAnimating()
        
        getDataOrder()
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func searchAction(_ sender: UIButton) {
        viewSearchContainer.isHidden = false
        tfSearch.becomeFirstResponder()
    }
    
    @IBAction func closeSearchAction(_ sender: UIButton) {
        viewSearchContainer.isHidden = true
        
        if tfSearch.text?.characters.count == 0 {
            listItem = temps
        }
    }
    
    @IBAction func orderAction(_ sender: UIButton) {
        let filteredArray = temps.filter {
            $0.qtyGet > 0
        }
        
        if filteredArray.count > 0 {
            let storyboard = UIStoryboard(name: REST.StoryboardReferences.order, bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.Order.orderSummary) as! OrderSummaryViewController
            vc.listItem = filteredArray
            
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // create failed info to user
            let alert = JDropDownAlert(position: .top)
            alert.alertWith("Silahkan pilih produk terlebih dahulu.", delay: 2)
        }
    }
    
    @IBAction func tfChangeText(_ sender: UITextField) {
        print(tfSearch.text!.lowercased())
        checkSearch(txt: tfSearch.text!.lowercased())
    }
    
    func getDataOrder() {
        OrderController().getListItem(onSuccess: { (code, message, results) in
            guard let res = results else { return }
            
            self.listItem = res
        }, onFailed: { (message) in
            print(message)
            print("Do action when data failed to fetching here")
        }) { (message) in
            print(message)
            print("Do action when data complete to fetching here")
            
            self.lblSearch.isHidden = true
            self.constraintLblSearch.constant = 0
            self.indicatorView.stopAnimating()
            self.table.isHidden = false
        }
    }
    
    func checkSearch(txt: String) {
        listItem = []
        
        for dest in temps {
            if dest.namaItem.lowercased().contains(txt) || dest.subKategoriCode.lowercased().contains(txt) || dest.barcode.lowercased().contains(txt) {
                listItem.append(dest)
            }
        }
        
        table.reloadData()
    }

}

extension ListItemViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ListItemCell.identifier, for: indexPath) as! ListItemCell
        
        let data = listItem[indexPath.row]
        
        cell.lblProdName.text = data.namaItem
        cell.lblProdName.sizeToFit()
        
        cell.lblGroup.text = data.kategoriItem
        cell.lblQty.text = "Stock : \(data.qty) Pcs"
        cell.lblQtyGet.text = "\(data.qtyGet)"
        
        cell.imgProd.clipsToBounds = true
        cell.imgProd.sd_setImage(with: URL.init(string: "\(data.imgName.basePictureURL)"), placeholderImage: UIImage.init(named: "no-image.png"))
        
        cell.tfNote.placeHolderText = "Note"
        cell.tfNote.type = .standard
        cell.tfNote.style = ListItemTextInputStyle() as AnimatedTextInputStyle
        cell.tfNote.delegate = self
        
        cell.tfNote.text = data.note
        
        cell.constraintHeightNote.constant = data.qtyGet > 0 ? 40 : 0
        
        cell.delegate = self
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension ListItemViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 170
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

extension ListItemViewController: listItemCellDelegate {
    func minusQty(cell: ListItemCell) {
        let indexPath = table.indexPath(for: cell)
        
        var qty = 0
        
        if cell.lblQtyGet.text != "0" {
            qty = Int(cell.lblQtyGet.text!)! - 1
            
            cell.lblQtyGet.text = "\(qty)"
        }
        
        if qty < 1 {
            table.beginUpdates()
            cell.constraintHeightNote.constant = 0
            table.endUpdates()
        }
        
        listItem[(indexPath?.row)!].qtyGet = qty
    }
    
    func plusQty(cell: ListItemCell) {
        let indexPath = table.indexPath(for: cell)
        
        let qty = Int(cell.lblQtyGet.text!)! + 1
        
        cell.lblQtyGet.text = "\(qty)"
        
        listItem[(indexPath?.row)!].qtyGet = qty
        
        table.beginUpdates()
        cell.constraintHeightNote.constant = 40
        table.endUpdates()
    }
}

extension ListItemViewController: AnimatedTextInputDelegate {
    
    func animatedTextInputDidEndEditing(animatedTextInput: AnimatedTextInput) {
        let pointInTable = animatedTextInput.convert(animatedTextInput.bounds.origin, to: table)
        let indexPath = table.indexPathForRow(at: pointInTable)
        
        listItem[(indexPath?.row)!].note = animatedTextInput.text!
    }
    
}
