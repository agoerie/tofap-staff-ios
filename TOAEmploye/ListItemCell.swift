//
//  ListItemCell.swift
//  TOAEmploye
//
//  Created by Agoerie on 2/24/18.
//  Copyright © 2018 ykdigital. All rights reserved.
//

import Foundation
import SDWebImage
import AnimatedTextInput

protocol listItemCellDelegate: class {
    func plusQty(cell: ListItemCell)
    func minusQty(cell: ListItemCell)
}

class ListItemCell: UITableViewCell {
    
    @IBOutlet var imgProd: UIImageView!
    @IBOutlet var lblProdName: UILabel!
    @IBOutlet var lblGroup: UILabel!
    @IBOutlet var lblQty: UILabel!
    @IBOutlet var lblQtyGet: UILabel!
    @IBOutlet var tfNote: AnimatedTextInput!
    @IBOutlet var constraintHeightNote: NSLayoutConstraint!
    
    open weak var delegate: listItemCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        super.selectionStyle = .none
    }
    
    @IBAction func upAction(_ sender: UIButton) {
        delegate?.plusQty(cell: self)
    }
    
    @IBAction func downAction(_ sender: UIButton) {
        delegate?.minusQty(cell: self)
    }
    
}

extension ListItemCell: TableViewCellProtocol {
    
    static func configure<T>(context: UIViewController, tableView: UITableView, indexPath: IndexPath, object: T) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ListItemCell.identifier, for: indexPath) as! ListItemCell
        
        return cell
    }
    
}

struct ListItemTextInputStyle: AnimatedTextInputStyle {
    let placeholderInactiveColor = UIColor.white
    let activeColor = UIColor.gray.withAlphaComponent(0.5)
    let inactiveColor = UIColor.gray.withAlphaComponent(0.5)
    let lineInactiveColor = UIColor.gray.withAlphaComponent(0.2)
    let lineActiveColor = UIColor.white.withAlphaComponent(1.0)
    let lineHeight: CGFloat = 1
    let errorColor = UIColor.red
    let textInputFont = UIFont.systemFont(ofSize: 13)
    let textInputFontColor = UIColor.gray
    let placeholderMinFontSize: CGFloat = 9
    let counterLabelFont: UIFont? = UIFont.systemFont(ofSize: 9)
    let leftMargin: CGFloat = 0
    let topMargin: CGFloat = 15
    let rightMargin: CGFloat = 0
    let bottomMargin: CGFloat = 0
    let yHintPositionOffset: CGFloat = 7
    let yPlaceholderPositionOffset: CGFloat = 0
    public let textAttributes: [String: Any]? = nil
}
