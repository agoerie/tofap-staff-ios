//
//  MenuBarCell.swift
//  TOAEmploye
//
//  Created by Agoerie on 2/28/18.
//  Copyright © 2018 ykdigital. All rights reserved.
//

import UIKit

class MenuBarCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        super.selectionStyle = .none
    }

}

extension MenuBarCell: TableViewCellProtocol {
    static func configure<T>(context: UIViewController, tableView: UITableView, indexPath: IndexPath, object: T) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MenuBarCell.identifier, for: indexPath) as! MenuBarCell
        
        guard let data = object as? [String] else { return cell }
        
        cell.lblTitle.text = data[indexPath.row]
        
        return cell
    }
}
