//
//  ListOrder.swift
//  TOAEmploye
//
//  Created by Agoerie on 2/20/18.
//  Copyright © 2018 ykdigital. All rights reserved.
//

import Foundation
import SwiftyJSON

class ListOrder {
    var idPemesanan: Int
    var idPemesan: Int
    var noPemesanan: String
    var tglPemesanan: String
    var status: Int
    var pemesan : String
    var idKurir : Int
    var kurir : String
    var userType : String
    var totalBarang : Int
    var username : String
    var updateBy : String
    var txtStat : String
    var itemList : String
    var rate : Int
    
    init(idPemesanan: Int,idPemesan: Int,noPemesanan: String,tglPemesanan: String,status: Int,pemesan: String,idKurir: Int,kurir: String,userType: String,totalBarang: Int,username: String,updateBy: String,txtStat: String,itemList: String,rate: Int) {
        self.idPemesanan = idPemesanan
        self.idPemesan = idPemesan
        self.noPemesanan = noPemesanan
        self.tglPemesanan = tglPemesanan
        self.status = status
        self.pemesan = pemesan
        self.idKurir = idKurir
        self.kurir = kurir
        self.userType = userType
        self.totalBarang = totalBarang
        self.username = username
        self.updateBy = updateBy
        self.txtStat = txtStat
        self.itemList = itemList
        self.rate = rate
    }
    
    convenience init(json: JSON){
        let idPemesanan = json["id_pemesanan"].intValue
        let idPemesan = json["id_pemesan"].intValue
        let noPemesanan = json["no_pemesanan"].stringValue
        let tglPemesanan = json["tgl_pemesanan"].stringValue
        let status = json["status"].intValue
        let pemesan = json["pemesan"].stringValue
        let idKurir = json["id_kurir"].intValue
        let kurir = json["kurir"].stringValue
        let userType = json["user_type"].stringValue
        let totalBarang = json["total_barang"].intValue
        let username = json["username"].stringValue
        let updateBy = json["update_by"].stringValue
        let txtStat = json["txt_stat"].stringValue
        let itemList = json["item_list"].stringValue
        let rate = json["rate"].intValue
        
        self.init(idPemesanan: idPemesanan, idPemesan: idPemesan, noPemesanan: noPemesanan, tglPemesanan: tglPemesanan, status: status, pemesan: pemesan, idKurir: idKurir, kurir: kurir, userType: userType, totalBarang: totalBarang, username: username, updateBy: updateBy, txtStat: txtStat, itemList: itemList, rate: rate)
    }
    
}
