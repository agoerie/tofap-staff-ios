//
//  GroupViewController.swift
//  TOAEmploye
//
//  Created by Agoerie on 3/3/18.
//  Copyright © 2018 ykdigital. All rights reserved.
//

import UIKit

protocol groupViewDelegate: class {
    func onDismissGroup(controller: GroupViewController)
    func onSelectMenuGroup(controller: GroupViewController,menu: String,id: Int)
}

class GroupViewController: UIViewController {
    
    @IBOutlet weak var table: UITableView!{
        didSet{
            let xib = GroupCell.nib
            table.register(xib, forCellReuseIdentifier: GroupCell.identifier)
            
            table.delegate = self
            table.dataSource = self
            
            table.separatorStyle = .none
            table.tableFooterView = UIView()
        }
    }
    
    @IBOutlet var tableConstraint: NSLayoutConstraint!
    
    open weak var delegate: groupViewDelegate?
    
    var listGroup = [ListGroup]() {
        didSet {
            temps = listGroup
            table.reloadData()
        }
    }
    
    var temps :[ListGroup] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableConstraint.constant = 0
        
        getDataGroup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func closeAction(_ sender: UIButton) {
        delegate?.onDismissGroup(controller: self)
    }
    
    func getDataGroup() {
        MenuController().getListGroup(onSuccess: { (code, message, results) in
            guard let res = results else { return }
            
            self.listGroup = res
        }, onFailed: { (message) in
            print(message)
            print("Do action when data failed to fetching here")
        }) { (message) in
            print(message)
            print("Do action when data complete to fetching here")
            let heights = self.listGroup.count * 45
            self.tableConstraint.constant = CGFloat(heights > 422 ? 422 : heights)
        }
    }

}

extension GroupViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listGroup.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = listGroup[indexPath.row]
        
        return GroupCell.configure(context: self, tableView: tableView, indexPath: indexPath, object: data)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = listGroup[indexPath.row]
        
        delegate?.onSelectMenuGroup(controller: self, menu: data.groupName, id: data.id)
    }
}

extension GroupViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
}
