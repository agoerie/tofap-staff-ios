//
//  OrderSummaryCell.swift
//  TOAEmploye
//
//  Created by Agoerie on 2/26/18.
//  Copyright © 2018 ykdigital. All rights reserved.
//

import UIKit

class OrderSummaryCell: UITableViewCell {

    @IBOutlet var imgProduct: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblQty: UILabel!
    @IBOutlet var lblDesc: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}

extension OrderSummaryCell: TableViewCellProtocol {
    
    static func configure<T>(context: UIViewController, tableView: UITableView, indexPath: IndexPath, object: T) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: OrderSummaryCell.identifier, for: indexPath) as! OrderSummaryCell
        
        guard let data = object as? ListItem else { return cell }
        
        cell.lblName.text = data.namaItem
        cell.lblName.sizeToFit()
        
        cell.lblQty.text = "Qty: \(data.qtyGet)"
        
        cell.lblDesc.text = "Note: \(data.note)"
        
        cell.imgProduct.clipsToBounds = true
        cell.imgProduct.sd_setImage(with: URL.init(string: "\(data.imgName.basePictureURL)"), placeholderImage: UIImage.init(named: "no-image.png"))
        
        return cell
    }
    
}
