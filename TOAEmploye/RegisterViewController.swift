//
//  RegisterViewController.swift
//  TOAEmploye
//
//  Created by Agoerie on 2/17/18.
//  Copyright © 2018 ykdigital. All rights reserved.
//

import UIKit
import AnimatedTextInput

class RegisterViewController: UIViewController {

    @IBOutlet var username: AnimatedTextInput!
    @IBOutlet var name: AnimatedTextInput!
    @IBOutlet var email: AnimatedTextInput!
    @IBOutlet var group: AnimatedTextInput!
    @IBOutlet var floor: AnimatedTextInput!
    @IBOutlet var closeImg: UIImageView!
    @IBOutlet var registerBtn: UIButton!
    
    var activityIndicator: UIActivityIndicatorView?
    
    var idGroup: Int?
    var idFloor: Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setupView() {
        let gesture = UITapGestureRecognizer.init(target: self, action: #selector(resignResponder))
        view.addGestureRecognizer(gesture)
        gesture.cancelsTouchesInView = false
        
        registerBtn.layer.cornerRadius = 22.5
        
        username.placeHolderText = "Username"
        username.type = .standard
        username.style = CustomTextInputStyle() as AnimatedTextInputStyle
        
        name.placeHolderText = "Name"
        name.type = .standard
        name.style = CustomTextInputStyle() as AnimatedTextInputStyle
        
        email.placeHolderText = "Email"
        email.type = .email
        email.style = CustomTextInputStyle() as AnimatedTextInputStyle
        
        group.placeHolderText = "Group"
        group.type = .standard
        group.tag = 1
        group.delegate = self
        group.style = CustomTextInputStyle() as AnimatedTextInputStyle
        
        floor.placeHolderText = "Floor"
        floor.type = .standard
        floor.tag = 2
        floor.delegate = self
        floor.style = CustomTextInputStyle() as AnimatedTextInputStyle
        
        closeImg.image = UIImage.init(named: "ic-close")?.withRenderingMode(.alwaysTemplate)
        closeImg.tintColor = UIColor.white
        
        activityIndicator = UIActivityIndicatorView.init(activityIndicatorStyle: .whiteLarge)
        activityIndicator?.center = view.center
        activityIndicator?.hidesWhenStopped = true
        activityIndicator?.stopAnimating()
        
        view.addSubview(activityIndicator!)
    }

    @IBAction func registerAction(_ sender: UIButton) {
        if idGroup == nil {
            let alert = UIAlertController.init(title: "Warning", message: "Please fill group field.", preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            return
        }
        
        activityIndicator?.startAnimating()
        registerBtn.isEnabled = false
        
        let param = ["username":username.text!,"name":name.text!,"email":email.text!,"group":"\(idGroup!)","lantai":"\(idFloor)"]
        
        UserController().postRegister(param: param, onSuccess: { (code, message, results) in
            print(message)
            print("Do action when data success to fetching here")
            self.activityIndicator?.stopAnimating()
            self.registerBtn.isEnabled = true
            
            let alert = UIAlertController.init(title: "Sukses", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: { (alert) in
                self.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }, onFailed: { (message) in
            print(message)
            print("Do action when data failed to fetching here")
            self.activityIndicator?.stopAnimating()
            self.registerBtn.isEnabled = true
            
            let alert = UIAlertController.init(title: nil, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }) { (message) in
            print(message)
            print("Do action when data complete to fetching here")
        }
    }
    
    @IBAction func closeAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func resignResponder() {
        self.view.endEditing(true)
    }
    
    func selectFloor() {
        let storyboard = UIStoryboard(name: REST.StoryboardReferences.main, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.Menu.floor) as! FloorViewController
        vc.delegate = self
        
        self.addChildViewController(vc)
        vc.didMove(toParentViewController: vc)
        vc.view.frame = UIScreen.main.bounds
        vc.view.layoutIfNeeded()
        vc.view.backgroundColor = UIColor.clear
        self.view.addSubview(vc.view)
        self.view.bringSubview(toFront: vc.view)
    }
    
    func selectGroup() {
        let storyboard = UIStoryboard(name: REST.StoryboardReferences.main, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.Menu.group) as! GroupViewController
        vc.delegate = self
        
        self.addChildViewController(vc)
        vc.didMove(toParentViewController: vc)
        vc.view.frame = UIScreen.main.bounds
        vc.view.layoutIfNeeded()
        vc.view.backgroundColor = UIColor.clear
        self.view.addSubview(vc.view)
        self.view.bringSubview(toFront: vc.view)
    }
    
}

extension RegisterViewController: AnimatedTextInputDelegate {
    func animatedTextInputShouldBeginEditing(animatedTextInput: AnimatedTextInput) -> Bool {
        if animatedTextInput.tag == 1 {
            selectGroup()
        }
        
        if animatedTextInput.tag == 2 {
            selectFloor()
        }
        
        return false
    }
}

extension RegisterViewController: floorViewDelegate {
    func onDismiss(controller: FloorViewController) {
        controller.willMove(toParentViewController: nil)
        controller.view.removeFromSuperview()
    }
    
    func onSelectMenu(controller: FloorViewController, menu: String, id: Int) {
        controller.willMove(toParentViewController: nil)
        controller.view.removeFromSuperview()
        
        floor.text = menu
        idFloor = id
    }
}

extension RegisterViewController: groupViewDelegate {
    func onDismissGroup(controller: GroupViewController) {
        controller.willMove(toParentViewController: nil)
        controller.view.removeFromSuperview()
    }
    
    func onSelectMenuGroup(controller: GroupViewController, menu: String, id: Int) {
        controller.willMove(toParentViewController: nil)
        controller.view.removeFromSuperview()
        
        group.text = menu
        idGroup = id
    }
}
